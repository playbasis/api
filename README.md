# API

## 3.0 12 APR 2020

- Implemented EmailOTP API (ALPHA)

```
POST {{hostname}}/Player/auth/{{playerId}}/requestEmailOTPCode HTTP/1.1
Accept: application/json
Content-Type: {{contentType}}
token={{token}}
```

```
POST {{hostname}}/Player/auth/{{playerId}}/verifyOTPCode HTTP/1.1
Accept: application/json
Content-Type: {{contentType}}

token={{token}}
&code={{code}}
&client_task_id={{client_task_id}}
```
