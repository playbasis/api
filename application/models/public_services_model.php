<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_services_model extends MY_Model
{

    public function logSignUpRequest($client_id, $site_id, $cl_player_id, $client_task_id, $player_id, $username)
    {    
        $this->mongo_db->insert('playbasis_singup_log', array(
            'client_id' => $client_id, // MongoId
            'site_id' => $site_id, // MongoId
            'cl_player_id' => $cl_player_id, // MongoId
            'client_task_id' => $client_task_id,
            'player_id' => $player_id,
            'username' => $username,
            'date_added' => new MongoDate(),
        ));
    }

    public function isOldSignUpRequest($client_task_id)
    {    
        $this->mongo_db->where('client_task_id', $client_task_id);
        $this->mongo_db->limit(1);
        $rows = $this->mongo_db->get('playbasis_singup_log');
        return $rows ? $rows[0] : null;
    }

    public function logTokenRevokeRequest($client_id, $site_id, $client_task_id, $player_id)
    {    
        $this->mongo_db->insert('playbasis_token_revoke_log', array(
            'client_id' => $client_id, // MongoId
            'site_id' => $site_id, // MongoId
            'client_task_id' => $client_task_id,
            'player_id' => $player_id,
            'date_added' => new MongoDate(),
        ));
    }

    public function isOldTokenRevokeRequest($client_task_id)
    {    
        $this->mongo_db->where('client_task_id', $client_task_id);
        $this->mongo_db->limit(1);
        $rows = $this->mongo_db->get('playbasis_token_revoke_log');
        return $rows ? $rows[0] : null;
    }

    public function registerGeneratedEmailOTP($email, $code, $client_task_id)
    {    
        $this->mongo_db->insert('playbasis_email_otpcode', array(
            'email' => $email,
            'client_task_id' => $client_task_id,
            'code' => $code,
            'status' => true,
            'date_added' => new MongoDate(),
            'date_expire' => new MongoDate(time() + SMS_VERIFICATION_TIMEOUT_IN_SECONDS),
        ));
    }

    public function registerGeneratedSMSOTP($phone_number, $code, $client_task_id)
    {    
        $this->mongo_db->insert('playbasis_sms_otpcode', array(
            'phone_number' => $phone_number,
            'client_task_id' => $client_task_id,
            'code' => $code,
            'status' => true,
            'date_added' => new MongoDate(),
            'date_expire' => new MongoDate(time() + SMS_VERIFICATION_TIMEOUT_IN_SECONDS),
        ));
    }

    public function checkEmailAvailability($email)
    {    
        $this->mongo_db->where('email', $email);
        $this->mongo_db->limit(1);
        $ids = $this->mongo_db->get('playbasis_player');
        return ($ids) ? $ids[0] : null;
    }

    public function validateEmailOTPCode($email, $code, $client_task_id)
    {    
        $this->mongo_db->where('email', $email);
        $this->mongo_db->where('code', $code);
        $this->mongo_db->where('status', true);
        $this->mongo_db->limit(1);
        $rows = $this->mongo_db->get('playbasis_email_otpcode');
        return $rows ? $rows[0] : null;
    }

    public function validateSMSOTPCode($phone_number, $code, $client_task_id)
    {    
        $this->mongo_db->where('phone_number', $phone_number);
        $this->mongo_db->where('code', $code);
        $this->mongo_db->where('status', true);
        $this->mongo_db->limit(1);
        $rows = $this->mongo_db->get('playbasis_sms_otpcode');
        return $rows ? $rows[0] : null;
    }

    public function markInvalidStateEmailOTP($email, $code, $client_task_id)
    {    
        $this->mongo_db->where(array(
            "email" => $email,
            "code" => $code,
        ))->set(array(
            "status" => false
        ))->update("playbasis_email_otpcode");
    }

    public function markInvalidStateSMSOTP($phone_number, $code, $client_task_id)
    {    
        $this->mongo_db->where(array(
            "phone_number" => $phone_number,
            "code" => $code,
        ))->set(array(
            "status" => false
        ))->update("playbasis_sms_otpcode");
    }

    
}
?>