<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_model extends MY_Model
{
    public function rewards() {
        return $this->mongo_db->get("rule_rewards");
    }
}