<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('mongo_db');
    }

    public function create($strCollectionName, $objCreationDoc)
    {
        return $this->mongo_db->insert(
            $strCollectionName,
            $objCreationDoc
        );
    }

    public function update($strCollectionName, $oid, $fieldName, $fieldValue)
    {
        $this->mongo_db->where(array('_id' => new MongoId($oid)));
        $this->mongo_db->set($fieldName, $fieldValue);
        return $this->mongo_db->update($strCollectionName);
    }

    public function read($strCollectionName, $objQuery, $intLimit)
    {
        $this->mongo_db->where($objQuery);
        $this->mongo_db->limit($intLimit);
        return $this->mongo_db->get($strCollectionName);
    }

    public function readOne($strCollectionName, $strOid)
    {
        $this->mongo_db->where(array('_id' => new MongoId($strOid)));
        $this->mongo_db->limit(1);
        return $this->mongo_db->get($strCollectionName);
    }
}
