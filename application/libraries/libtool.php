<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LibTool {

    protected $CI;

    public function __construct()
    { 
        $this->CI =& get_instance();
        $this->CI->load->model('tool_model');
    }

    public function log($msg = 'undefined') 
    {
        file_put_contents(
            'php://stderr',
            print_r(
                PHP_EOL
                .date('d/m/Y H:i:s',time())
                .' ; '
                .$msg
                .PHP_EOL
                ,TRUE
            )
        );
    }

    public function generateToken($userDefinedKey, $userDefinedSecrete)
    {

        $strGeneratedId = $this->libtool->mongoIdCreate('');
        $this->libtool->log('sd');

        echo "userDefinedKey = {$userDefinedKey}".PHP_EOL;
        echo "userDefinedSecrete = {$userDefinedSecrete}".PHP_EOL;

        $generatedRandomInt = rand(1000000, getrandmax());
        // https://www.php.net/manual/en/function.rand.php
        
        echo "generatedRandomInt = {$generatedRandomInt}".PHP_EOL;

        //$newToken = $this->token($data['key'], $data['secret'], $generatedRandomInt << 1);

        $hashStr= hash('sha512', 'hello');
        // https://www.php.net/manual/en/function.hash.php
        echo "hashStr = {$hashStr}".PHP_EOL;

        $this->libtool->log('sd');
    }

    public function mCreate($strCollectionName, $strJSONObject)
    {
        $result = (object)[];
        $result->done = false;
        
        $objCreationDoc = json_decode($strJSONObject, true);
        $oid =  $this->CI->tool_model->create($strCollectionName, $objCreationDoc);
        $result->done = isset($oid) == true ? true : false;
        $result->_id = $oid;
        return $result;
    }

    public function mUpdate($strCollectionName, $oid, $fieldName, $fieldValue)
    {
        $result = (object)[];
        $result->done = false;

        $oneIsSuccess = $this->CI->tool_model->update($strCollectionName, $oid, $fieldName, $fieldValue);
        $result->done = $oneIsSuccess == 1 ? true : false;
        return $result;
    }

    public function mRead($strCollectionName, $strQuery, $intLimit)
    {
        $result = (object)[];
        $result->done = false;

        $objQuery = json_decode(urldecode($strQuery), true);
        $docs = $this->CI->tool_model->read($strCollectionName, $objQuery, $intLimit);
        $result->done = true;
        $result->name = $strCollectionName;
        $result->count = count($docs);
        $result->docs = $docs;
        return $result; 
    }

    public function mReadOne($strCollectionName, $strOid)
    {
        $result = (object)[];
        $result->done = false;

        $docs = $this->CI->tool_model->readOne($strCollectionName, $strOid);
        $result->done = true;
        $result->name = $strCollectionName;
        $result->count = count($docs);
        $result->doc = $result->count == 1 ? $docs[0] : null;
        return $result; 
    }

}