<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';

class Public_services extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('player_model');
        $this->load->model('email_model');
        $this->load->model('client_model');
        $this->load->model('sms_model');
        $this->load->model('setting_model');
        $this->load->model('public_services_model');
        $this->load->model('tool/utility', 'utility');
        $this->load->model('tool/error', 'error');
        $this->load->model('tool/respond', 'resp');
    }

        /**
     * @OA\Get(
     *     tags={"PublicServices"},
     *     path="/emailAvailability",
     *     description="Check Email Availability",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="cid",
     *         in="body",
     *         description="cid",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="sid",
     *         in="body",
     *         description="sid",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="email",
     *         in="body",
     *         description="to email",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="session",
     *         in="body",
     *         description="session",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function emailAvailability_post() {
        $cid = $this->input->post("cid");
        $sid = $this->input->post("sid");

        if (!$this->client_model->hasSid($cid, $sid)) {
            $this->response($this->error->setError('CANNOT_FIND_CLIENT_ID'), 200);
        }

        $email = $this->input->post("email");
        $session = $this->input->post("session");

        header('Access-Control-Allow-Origin: *'); 

        $result = $this->public_services_model->checkEmailAvailability($email);
        if (!$result) {
            $this->response(
                $this->resp->setRespond(
                    array('result' => md5($session."NF")."NF".md5(time()))), 200);
        }

        $this->response(
            $this->resp->setRespond(
                array('result' => md5($session."OK").md5(time()))), 200);
    }


    /**
     * @OA\Get(
     *     tags={"PublicServices"},
     *     path="/requestEmailOTP",
     *     description="Request Email OTP",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="csid",
     *         in="body",
     *         description="csid",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="transaction_code",
     *         in="body",
     *         description="transaction_code",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="client_task_id",
     *         in="body",
     *         description="client_task_id",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="email",
     *         in="body",
     *         description="email",
     *         required=true,
     *     )
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function requestEmailOTP_post() {
        header('Access-Control-Allow-Origin: *'); 

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $email = $this->input->post("email");

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("email", $email);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $isOldClientTaskId = $this->email_model->isOldClientTaskId(new MongoId($sid), $client_task_id);
        if ($isOldClientTaskId) {
            $this->response($this->error->setError('ALREADY_PROCESSED_TRANSACTION'), 200);
        }

        $code = $this->generateEmailOTPCode($email, $client_task_id);
        $from = "noreply@playbasis.com";        
        $subject = $code." is your OTP";
        $message = $code." is your OTP";
        $sent = $this->processEmail($from, $email, $subject, $message);
        if ($sent) {
            $this->email_model->logWithClientTaskId(
                "emailotp",
                new MongoId($cid),
                new MongoId($sid),
                $client_task_id,
                "response",
                "playerbasis",
                $email,
                $subject,
                $message,
                null,
                null,
                null,
                null);
        }
        
        $this->response($this->resp->setRespond(array('result' => "done")), 200);
    }

    /**
     * @OA\Get(
     *     tags={"PublicServices"},
     *     path="/requestSMSOTP",
     *     description="Request SMS OTP",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="csid",
     *         in="body",
     *         description="csid",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="transaction_code",
     *         in="body",
     *         description="transaction_code",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="client_task_id",
     *         in="body",
     *         description="client_task_id",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="phone_number",
     *         in="body",
     *         description="phone_number",
     *         required=true,
     *     )
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function requestSMSOTP_post() {
        header('Access-Control-Allow-Origin: *');

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $phone_number = $this->input->post('phone_number');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("phone_number", $phone_number);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $isOldClientTaskId = $this->sms_model->isOldClientTaskId(new MongoId($sid), $client_task_id);
        if ($isOldClientTaskId) {
            $this->response($this->error->setError('ALREADY_PROCESSED_TRANSACTION'), 200);
        }

        $code = $this->generateSMSOTPCode($phone_number, $client_task_id);
        $to = $phone_number;
        $type = "register";
        $message = $code." is your verification code";
        $this->sendSMS($cid, $sid, $client_task_id, $type, $to, $message);
        $this->response(
            $this->resp->setRespond(array('result' => "done"))
                , 200);
    }

    /**
     * @OA\Get(
     *     tags={"PublicServices"},
     *     path="/verifyEmailOTP",
     *     description="Verify Email OTP",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="csid",
     *         in="body",
     *         description="csid",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="transaction_code",
     *         in="body",
     *         description="transaction_code",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="client_task_id",
     *         in="body",
     *         description="to client_task_id",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="email",
     *         in="body",
     *         description="email",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="code",
     *         in="body",
     *         description="code",
     *         required=true,
     *     )
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function verifyEmailOTP_post() {
        header('Access-Control-Allow-Origin: *');

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $email = $this->input->post('email');
        $code = $this->input->post('code');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("email", $email);
        $this->validateParam("code", $code);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $result = $this->public_services_model->validateEmailOTPCode($email, $code, $client_task_id);
        if (!$result) {
            $this->response(
                $this->resp->setRespond(
                    array('result' => md5($client_task_id.$code)."NF".md5(time()))), 200);
        }
        
        if ($result['date_expire']->sec <= time()) {
            $this->response(
                $this->resp->setRespond(
                    array('result' => md5($client_task_id.$code)."__".$result['date_expire']->sec."__"."DE".md5(time()))), 200);
        }

        $this->public_services_model->markInvalidStateEmailOTP($email, $code, $client_task_id);
        $this->response(
            $this->resp->setRespond(
                array('result' => md5($client_task_id.$code."OK").md5(time()))), 200);
    }


    /**
     * @OA\Get(
     *     tags={"PublicServices"},
     *     path="/verifySMSOTP",
     *     description="Verify SMS OTP",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="csid",
     *         in="body",
     *         description="csid",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="transaction_code",
     *         in="body",
     *         description="transaction_code",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="client_task_id",
     *         in="body",
     *         description="to client_task_id",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="phone_number",
     *         in="body",
     *         description="phone_number",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="code",
     *         in="body",
     *         description="code",
     *         required=true,
     *     )
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function verifySMSOTP_post() {
        header('Access-Control-Allow-Origin: *');

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $phone_number = $this->input->post('phone_number');
        $code = $this->input->post('code');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("phone_number", $phone_number);
        $this->validateParam("code", $code);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $result = $this->public_services_model->validateSMSOTPCode($phone_number, $code, $client_task_id);
        if (!$result) {
            $this->response(
                $this->resp->setRespond(
                    array('result' => md5($client_task_id.$code)."NF".md5(time()))), 200);
        }
        
        if ($result['date_expire']->sec <= time()) {
            $this->response(
                $this->resp->setRespond(
                    array('result' => md5($client_task_id.$code)."DE".md5(time()))), 200);
        }

        $this->public_services_model->markInvalidStateSMSOTP($phone_number, $code, $client_task_id);
        $this->response(
            $this->resp->setRespond(
                array('result' => md5($client_task_id.$code."OK").md5(time()))), 200);
    }

    public function sendEmail_post() {
        header('Access-Control-Allow-Origin: *');

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);  
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $to = $this->input->post('to');       
        $function = $this->input->post('function');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("to", $to);
        $this->validateParam("function", $function);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);
        $isOldClientTaskId = $this->email_model->isOldClientTaskId(new MongoId($sid), $client_task_id);
        if ($isOldClientTaskId) {
            $this->response($this->error->setError('ALREADY_PROCESSED_TRANSACTION'), 200);
        }

        if ($function == "WelcomeNewAccountEmail") {
            $template_id = "WelcomeNewAccountEmail";
            $subject = "Thanks for registering! Here’s a recap of what we have done";
            
            $email = $this->input->post('email');
            $player = $this->input->post('player');
            $this->validateParam("email", $email);
            $this->validateParam("player", $player);

            $contentCode = $this->input->post('contentCode');
            $this->validateParam("contentCode", $contentCode);
            $this->validateContentCode($contentCode,$email.$player, $client_task_id);

        } else if ($function == "SignUpInvitationEmail") {
            $template_id = "SignUpInvitationEmail";
            $subject = "Invitation to sign-up";
            $signUpCode = $this->input->post('s');
            $this->validateParam("s", $signUpCode);

            $contentCode = $this->input->post('contentCode');
            $this->validateParam("contentCode", $contentCode);
            $this->validateContentCode($contentCode,$signUpCode, $client_task_id);

        } else if ($function == "InviteFriendEmail") {
            $template_id = "InviteFriendEmail";
            $subject = "Invitation to sign-up";
            $referralCode = $this->input->post('r');
            $signUpCode = $this->input->post('s');
            $this->validateParam("s", $signUpCode);
            $this->validateParam("r", $referralCode);

            $contentCode = $this->input->post('contentCode');
            $this->validateParam("contentCode", $contentCode);
            $this->validateContentCode($contentCode,$signUpCode.$referralCode, $client_task_id);

        } else if ($function == "FeedbackEmail") {
            $template_id = "FeedbackEmail";
            $player = $this->input->post('player');
            $scale = $this->input->post('scale');
            $emoji = $this->input->post('emoji');
            $this->validateParam("player", $player);
            $this->validateParam("scale", $scale);
            $this->validateParam("emoji", $emoji);

            $contentCode = $this->input->post('contentCode');
            $this->validateParam("contentCode", $contentCode);
            $this->validateContentCode($contentCode,$player.$scale, $client_task_id);

            $subject = "Feedback by ".player;
        }else {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }

        // fixed
        $from = "noreply@playbasis.com";

        $template = $this->email_model->getTemplateByTemplateId($sid, $template_id);
        if (!$template) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
        $message = $template['body'];

        if ($signUpCode) {
            $message = str_replace("(s)", $signUpCode, $message);
        }

        if ($referralCode) {
            $message = str_replace("(r)", $referralCode, $message);
        }

        if ($email) {
            $message = str_replace("(email)", $email, $message);
        }

        if ($player) {
            $message = str_replace("(player)", $player, $message);
        }

        if ($scale) {
            $message = str_replace("(scale)", $scale, $message);
        }

        if ($emoji) {
            $message = str_replace("(emoji)", $emoji, $message);
        }
    
        $sent = $this->processEmail($from, $to, $subject, $message);
        if ($sent) {
            $this->email_model->logWithClientTaskId(
                $template_id,
                new MongoId($cid),
                new MongoId($sid),
                $client_task_id,
                "response",
                "playerbasis",
                $to,
                $subject,
                $message,
                null,
                null,
                null,
                null);
        }
        $this->response(
            $this->resp->setRespond(
                array('result' => "done")), 200);
    }


    public function validatePassword_post() {
        header('Access-Control-Allow-Origin: *'); 

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $password = $this->input->post('password');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("password", $password);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);
    }

    public function passwordPolicy_post() {
        header('Access-Control-Allow-Origin: *'); 

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $result = $this->setting_model->retrievePasswordPolicy(new MongoId($cid), new MongoId($sid));
        if ($result) {
            $this->response(
                $this->resp->setRespond(
                    array(
                        'policy' => 
                            $result["min_char"].md5(time().$result["min_char"])."&".
                            ($result["user_in_password"] == "true" ? "1" : "0").md5(time().$result["user_in_password"])."&".
                            ($result["prevent_password_reuse"] == "true" ? "1" : "0").md5(time().$result["prevent_password_reuse"])."&".
                            $this->getStrategyCode($result["strategy"]).md5(time().$result["strategy"])."&".
                            $this->get2FACode($result["signup_2fa"]).md5(time().$result["signup_2fa"])."&".
                            $this->get2FACode($result["signin_2fa"]).md5(time().$result["signin_2fa"]),
                    )), 200);
        }
    }

    //PRIVATE
    public function require_at_least_number($str) {
        if (preg_match('#[0-9]#', $str)) {
            return true;
        }
        $this->form_validation->set_message('require_at_least_number',
            'The %s field require at least one numberic character');
        return false;
    }

    //PRIVATE
    public function require_at_least_alphabet($str) {
        if (preg_match('#[a-zA-Z]#', $str)) {
            return true;
        }
        $this->form_validation->set_message('require_at_least_alphabet',
            'The %s field require at least one alphabet character');
        return false;
    }


    //PRIVATE
    private function getStrategyCode($text) {
        if ($text == "NOnly") {
            return "1";
        } else if ($text == "AOnly") {
            return "2";
        } else if ($text == "AtLeastOneN") {
            return "3";
        } else if ($text == "AtLeastOneA") {
            return "4";
        } else {
            return "9";
        }
    }

    //PRIVATE
    private function get2FACode($text) {
        if ($text == "None") {
            return "1";
        } else if ($text == "SMS") {
            return "2";
        } else if ($text == "Email") {
            return "3";
        } else {
            return "9";
        }
    }
    
    // PRIVATE
    private function generateEmailOTPCode($email, $client_task_id)
    {
        $code = $this->genCode(SMS_VERIFICATION_CODE_LENGTH, false, false, true);
        $this->public_services_model->registerGeneratedEmailOTP($email, $code, $client_task_id);
        return $code;
    }

    // PRIVATE
    private function generateSMSOTPCode($phone_number, $client_task_id)
    {
        $code = $this->genCode(SMS_VERIFICATION_CODE_LENGTH, false, false, true);
        $this->public_services_model->registerGeneratedSMSOTP($phone_number, $code, $client_task_id);
        return $code;
    }

    // PRIVATE
    private function genCode($length, $use_lower_case, $use_upper_case,  $use_numbers ){
        $code = get_random_code($length, $use_lower_case, $use_upper_case, $use_numbers);
        if (!$code) {
            throw new Exception('Cannot generate unique player code');
        }
        return $code;
    }

    // PRIVATE
    private function processEmail($from, $to, $subject, $message)
    {
        $response = $this->utility->email($from, $to, $subject, $message);
        if ($response == true) {
            return true;
        } else {
            $this->response($this->error->setError('CANNOT_SEND_EMAIL', implode(',', $to)), 200);
            return false;
        }
    }

    // PRIVATE
    private function sendSMS($cid, $sid, $client_task_id, $type, $to, $message)
    {
        $this->config->load("twilio", true);
        $config = $this->sms_model->getSMSClient(new MongoId($cid), new MongoId($sid));
        $twilio = $this->config->item('twilio');
        $config['api_version'] = $twilio['api_version'];
        $this->load->library('twilio/twiliomini', $config);
        $response = $this->twiliomini->sms( $config['number'], $to, $message);
        $this->sms_model->logWithClientTaskId($cid, $sid, $client_task_id, $type, $config['number'], $to, $message, $response);
        if ($response->IsError) {
            $this->response(
                $this->resp->setRespond(
                    array('result' => "invalid transaction".print_r($response, true))), 200);
        }
    }

    // PRIVATE
    private function validateCSID($cid, $sid) {
        if (!$this->client_model->hasSid($cid, $sid)) {
            $this->response($this->error->setError('CANNOT_FIND_CLIENT_ID'), 200);
        }
    }

    // PRIVATE
    private function validateParam($paramName, $paramValue) {
        if (!$paramValue) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }

    // PRIVATE
    private function validateTransaction($transaction_code, $cid, $sid, $client_task_id) {
        $validCode = md5($cid.$sid.$client_task_id);
        if ($transaction_code != $validCode) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }

    // PRIVATE
    private function validateContentCode($codeCode, $content, $client_task_id) {
        $validCode = md5($content.$client_task_id);
        if ($codeCode != $validCode) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }
}