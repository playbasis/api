<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';

class Public_configuration extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->model('custom_style_model');
        $this->load->model('tool/utility', 'utility');
        $this->load->model('tool/error', 'error');
        $this->load->model('tool/respond', 'resp');
    }

    /**
     * @OA\Get(
     *     tags={"Configuration"},
     *     path="/PublicConfiguration",
     *     description="Retrieve public configuration by specified filter fields",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="cid",
     *         in="query",
     *         description="cid",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="sid",
     *         in="query",
     *         description="sid",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="name",
     *         in="query",
     *         description="Name of group to retrieve configuration",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="key",
     *         in="query",
     *         description="Name of key name to retrieve configuration",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="sort",
     *         in="query",
     *         description="Field to sort (e.g. name, key, date_added, date_modified)",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         name="order",
     *         in="query",
     *         description="Field to sort (e.g. name, key, date_added, date_modified)",
     *         required=false,
     *         @OA\Schema(
     *             enum={"asc", "desc", "random"}
     *         )
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *         name="offset",
     *         in="query",
     *         description="Specify paging offset | default = 0",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *         name="limit",
     *         in="query",
     *         description="Specify paging limit | default = 20",
     *         required=false,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function list_post()
    {
        header('Access-Control-Allow-Origin: *'); 
        header_remove('Set-Cookie');

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $name = $this->input->post('name');
        $key = $this->input->post('key');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("name", $name);
        $this->validateParam("key", $key);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $query_data = array('name' =>$name, 'key' =>$key);
        $result = $this->custom_style_model->retrieveStyle(new MongoId($cid), new MongoId($sid), $query_data);
        array_walk_recursive($result, array($this, "convert_mongo_object_and_optional"));        
        $this->response($this->resp->setRespond(array('result' => $result)), 200);
    }

    /**
     * Use with array_walk and array_walk_recursive.
     * Recursive iterable items to modify array's value
     * from MongoId to string and MongoDate to readable date
     * @param string $key
     */
    private function convert_mongo_object_and_optional(&$item)
    {
        if (is_object($item)) {
            if (get_class($item) === 'MongoId') {
                $item = $item->{'$id'};
            } else {
                if (get_class($item) === 'MongoDate') {
                    $item = datetimeMongotoReadable($item);
                }
            }
        }
    }

     // PRIVATE
     private function validateCSID($cid, $sid) {
        if (!$this->client_model->hasSid($cid, $sid)) {
            $this->response($this->error->setError('CANNOT_FIND_CLIENT_ID'), 200);
        }
    }

    // PRIVATE
    private function validateParam($paramName, $paramValue) {
        if (!$paramValue) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }

    // PRIVATE
    private function validateTransaction($transaction_code, $cid, $sid, $client_task_id) {
        $validCode = md5($cid.$sid.$client_task_id);
        if ($transaction_code != $validCode) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }

}