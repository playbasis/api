<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST2_Controller.php';
class Integrator extends REST2_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('player_model');
        $this->load->library('form_validation');
        $this->load->library('parser');
        error_log("Integrator controller loaded");
    }

    private function milliseconds()
    {
        $mt = explode(' ', microtime());
        return ((int) $mt[1]) * 1000 + ((int) round($mt[0] * 1000));
    }

    private function debug($message)
    {
        file_put_contents('Integrator.log', $this->milliseconds() . ': ' . $message . PHP_EOL, FILE_APPEND);
    }

    public function hello_post()
    {
        $player_id = $_POST['player_id'];
        if (!$player_id) {
            $this->response($this->error->setError('PARAMETER_MISSING', array(
                'player_id'
            )), 200);
        }

        $this->debug("hello_post: validToken = " . $this->validToken["key"]);

        $this->debug("hello_post: player_id = " . $player_id);
        $player['player'] = $this->player_model->readPlayer($player_id, $this->site_id, array(
            'username',
            'first_name',
            'last_name',
            'gender',
            'tags',
            'image',
            'exp',
            'level',
            'date_added',
            'birth_date'
        ));
        echo "Hello hello_post" . $player_id;
    }
}
