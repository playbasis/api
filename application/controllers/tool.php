<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Tool extends CI_Controller {

    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('tool_model');
        $this->load->library('libtool');
    }

    protected function isMaintenanceEnv() {
        return getenv('MAINTENANCE');
    }

    // MAINTENANCE=true php index.php tool mCreate 'client' '{"name": "test", "age": 12}' 
    public function mCreate($strCollectionName, $strJSONObject)
    {
        if (!$this->isMaintenanceEnv()) {
            echo 'Access denied';
            return;
        }

        $result = $this
                    ->libtool
                    ->mCreate(
                        $strCollectionName,
                        $strJSONObject
                    );
        print_r($result);
        /*
        stdClass Object
        (
            [done] => 1
            [_id] => MongoId Object
                (
                    [$id] => 5ce6786671d9bfa8008b4567
                )

        )
        */
    }

    // MAINTENANCE=true php index.php tool mUpdate 'client' '5ce6775771d9bfa4008b4567' 'name' 'value' 
    public function mUpdate($strCollectionName, $strMongoId, $fieldName, $fieldValue)
    {
        if (!$this->isMaintenanceEnv()) {
            echo 'Access denied';
            return;
        }

        $result = $this
                    ->libtool
                    ->mUpdate(
                        $strCollectionName,
                        $strMongoId,
                        $fieldName,
                        $fieldValue
                    );
        print_r($result);
        /*
        stdClass Object
        (
            [done] => 1
        )
        */
    }

    public function wmRead($strCollectionName='untitled', $strQuery='{}', $intLimit=1)
    {
        if (!$this->isMaintenanceEnv()) {
            echo 'Access denied';
            return;
        }

        $result = $this
                    ->libtool
                    ->mRead(
                        $strCollectionName,
                        $strQuery,
                        $intLimit
                    );
        $this->load->view('tool/wmRead', $result);

    }

    // MAINTENANCE=true php index.php tool mRead 'client' '{}' 10 
    public function mRead($strCollectionName, $strQuery, $intLimit)
    {
        if (!$this->isMaintenanceEnv()) {
            echo 'Access denied';
            return;
        }

        $result = $this
                    ->libtool
                    ->mRead(
                        $strCollectionName,
                        $strQuery,
                        $intLimit
                    );
        print_r($result);
        /*
        stdClass Object
        (
            [done] => 1
            [docs] => Array
                (
                    [0] => Array
                        (
                            [_id] => MongoId Object
                                (
                                    [$id] => 5ce679a271d9bfaa008b4567
                                )

                            [name] => test
                            [age] => 12
                        )
                )

        )
        */
    }

     // MAINTENANCE=true php index.php tool mReadOne 'collectionName' '5ce6c55e52ee9b20008b4567' 
     public function mReadOne($strCollectionName, $strOid)
     {
        if (!$this->isMaintenanceEnv()) {
            echo 'Access denied';
            return;
        }

        $result = $this
                     ->libtool
                     ->mReadOne(
                        $strCollectionName,
                        $strOid
                     );
        print_r($result);
     }
}