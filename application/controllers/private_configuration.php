<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST2_Controller.php';

class Private_configuration extends REST2_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('custom_style_model');
        $this->load->model('tool/utility', 'utility');
        $this->load->model('tool/error', 'error');
        $this->load->model('tool/respond', 'resp');
    }

    /**
     * @OA\Get(
     *     tags={"Configuration"},
     *     path="/Configuration",
     *     description="Retrieve configuration by specified filter fields",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="name",
     *         in="query",
     *         description="Name of group to retrieve configuration",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="key",
     *         in="query",
     *         description="Name of key name to retrieve configuration",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="sort",
     *         in="query",
     *         description="Field to sort (e.g. name, key, date_added, date_modified)",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         name="order",
     *         in="query",
     *         description="Field to sort (e.g. name, key, date_added, date_modified)",
     *         required=false,
     *         @OA\Schema(
     *             enum={"asc", "desc", "random"}
     *         )
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *         name="offset",
     *         in="query",
     *         description="Specify paging offset | default = 0",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *         name="limit",
     *         in="query",
     *         description="Specify paging limit | default = 20",
     *         required=false,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function list_get()
    {
        $this->benchmark->mark('start');
        $query_data = $this->input->get(null, true);

        $result = $this->custom_style_model->retrieveStyle($this->validToken['client_id'],
            $this->validToken['site_id'], $query_data);

        array_walk_recursive($result, array($this, "convert_mongo_object_and_optional"));
        $this->benchmark->mark('end');
        $t = $this->benchmark->elapsed_time('start', 'end');
        $this->response($this->resp->setRespond(array('result' => $result, 'processing_time' => $t)), 200);
    }


    /**
     * Use with array_walk and array_walk_recursive.
     * Recursive iterable items to modify array's value
     * from MongoId to string and MongoDate to readable date
     * @param string $key
     */
    private function convert_mongo_object_and_optional(&$item)
    {
        if (is_object($item)) {
            if (get_class($item) === 'MongoId') {
                $item = $item->{'$id'};
            } else {
                if (get_class($item) === 'MongoDate') {
                    $item = datetimeMongotoReadable($item);
                }
            }
        }
    }
}