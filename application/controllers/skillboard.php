<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST2_Controller.php';

class Skillboard extends REST2_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('skillboard_model');
        $this->load->model('tool/utility', 'utility');
        $this->load->model('tool/error', 'error');
        $this->load->model('tool/respond', 'resp');
        $this->load->library('amazon_sqs');
    }

    public function createBoard_post()
    {
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $max_point_credit = $this->input->post('max_point_credit');

        if (!$name) {
            $this->response($this->error->setError('PARAMETER_MISSING', array(
                'name'
            )), 200);
        }

        if (!$description) {
            $this->response($this->error->setError('PARAMETER_MISSING', array(
                'description'
            )), 200);
        }

        if (!$max_point_credit) {
            $this->response($this->error->setError('PARAMETER_MISSING', array(
                'max_point_credit'
            )), 200);
        }

        $max_point_credit_int = intval($max_point_credit);
        if ($max_point_credit_int <= 0) {
            $this->response($this->error->setError('PARAMETER_MISSING', array(
                'max_point_credit must be greater than zero'
            )), 200);
        }

        $data = array(
            'client_id' => $this->client_id,
            'site_id' => $this->site_id,
            'name' => $name,
            'description' => $description,
            'max_point_credit' => $max_point_credit
        );
        $newSkillboard = $this->skillboard_model->createBoard($data);
        if ($newSkillboard) {
           
            $message = 'ABCD';
            $attributes = array(); // optional, 2d array of attributes. e.g. ["TicketSubject" => ['DataType' => "String",'StringValue' => "Some test ticket subject"], "TicketID" => ['DataType' => "Number", 'StringValue' => "12069607"]]
            $delay = 0; 
            $result = $this->amazon_sqs->createTicket($message, $attributes, $delay);
            //print_r($result);
            $this->response($this->resp->setRespond($newSkillboard), 200);

        } else {
            $this->response($this->error->setError('PARAMETER_MISSING', array(
                'newSkillboard'
            )), 200);
        }
    }

    public function getBoards_get()
    {

    }

    public function addBadgeToBoard_post()
    {

    }

    public function getBadgesFromBoard_get()
    {

    }

    public function addBadgeToPlayer_post()
    {

    }

    public function getPlayerBadgesFromBoard_get()
    {

    }
}