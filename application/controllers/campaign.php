<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST2_Controller.php';

class Campaign extends REST2_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('campaign_model');
        $this->load->model('player_model');
        $this->load->model('tool/error', 'error');
        $this->load->model('tool/respond', 'resp');
    }

    /**
     * @OA\Get(
     *     tags={"Campaign"},
     *     path="/Campaign",
     *     description="Get campaign",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="campaign_name",
     *         in="query",
     *         description="Name of campaign to retrieve campaign details",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="tags",
     *         in="query",
     *         description="Specific tag(s) to find",
     *         required=false,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function index_get()
    {
        $campaign_name = $this->input->get('campaign_name');
        $tags = $this->input->get('tags') ? explode(',', $this->input->get('tags')) : null;
        $result = $this->campaign_model->getCampaign($this->client_id, $this->site_id, $campaign_name ? $campaign_name: false, $tags);
        foreach ($result as $index => $res){
           unset($result[$index]['_id']);
        }
        array_walk_recursive($result, array($this, "convert_mongo_object_and_optional"));
        $this->response($this->resp->setRespond($result), 200);
    }

    public function addPerformer_post()
    {
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $validCode = md5($this->client_id.$this->site_id.$client_task_id);
        if ($transaction_code == $validCode) {
            $campaign_name = $this->input->post('campaign_name');
            $username = $this->input->post('username');
            $score = $this->input->post('score');
            $isSuccess = $this->campaign_model->addCampaignPerformer($this->client_id, $this->site_id, $campaign_name, $username, $score);
            $this->response(
                $this->resp->setRespond(array('result' => "done"))
                    , 200);
        } else {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }


    /**
     * @OA\Get(
     *     tags={"Campaign"},
     *     path="/Campaign/active",
     *     description="Retrieve active campaign",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="tags",
     *         in="query",
     *         description="Specific tag(s) to find",
     *         required=false,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function activeCampaign_get()
    {
        $tags = $this->input->get('tags') ? explode(',', $this->input->get('tags')) : null;
        $result = $this->campaign_model->getActiveCampaign($this->client_id, $this->site_id, $tags);
        if($result){
            unset($result['_id']);
            array_walk_recursive($result, array($this, "convert_mongo_object_and_optional"));
        }

        $this->response($this->resp->setRespond($result), 200);
    }

    /**
     * Use with array_walk and array_walk_recursive.
     * Recursive iterable items to modify array's value
     * from MongoId to string and MongoDate to readable date
     * @param mixed $item this is reference
     * @param string $key
     */
    private function convert_mongo_object_and_optional(&$item, $key)
    {
        if (is_object($item)) {
            if (get_class($item) === 'MongoId') {
                $item = $item->{'$id'};
            } else {
                if (get_class($item) === 'MongoDate') {
                    $item = datetimeMongotoReadable($item);
                }
            }
        }
    }
}