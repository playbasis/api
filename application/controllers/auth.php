<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST2_Controller.php';

class Auth extends REST2_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('client_model');
        $this->load->model('energy_model');
        $this->load->model('player_model');
        $this->load->model('setting_model');
        $this->load->model('public_services_model');
        $this->load->model('tool/error', 'error');
        $this->load->model('tool/utility', 'utility');
        $this->load->model('tool/respond', 'resp');
        $this->load->library('form_validation');
    }

    /**
     * @todo Get api_key and api_secret from params, not formData
     * @OA\Post(
     *     tags={"Auth"},
     *     path="/Auth",
     *     description="Request an access token from Playbasis",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *                @OA\Property(
     *                    property="api_key",
     *                    type="string",
     *                    description="API Key issued by Playbasis",
     *                ),
     *                @OA\Property(
     *                    property="api_secret",
     *                    type="string",
     *                    description="API Secret issued by Playbasis",
     *                ),
     *            ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function index_post()
    {
        $required = $this->input->checkParam(array(
            'api_key',
            'api_secret'
        ));
        if ($required) {
            $this->response($this->error->setError('PARAMETER_MISSING', $required), 200);
        }
        $API['key'] = $this->input->post('api_key');
        $API['secret'] = $this->input->post('api_secret');
        $clientInfo = $this->auth_model->getApiInfo($API);
        if ($clientInfo) {
            $token = $this->auth_model->generateToken(array_merge($clientInfo, $API));
            $this->response($this->resp->setRespond($token), 200);
        } else {
            $this->response($this->error->setError('INVALID_API_KEY_OR_SECRET', $required), 200);
        }
    }

    /**
     * @todo Get api_key and api_secret from params, not formData
     * @OA\Post(
     *     tags={"Auth"},
     *     path="/Auth/renew",
     *     description="Create a new access token from Playbasis",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *                @OA\Property(
     *                    property="api_key",
     *                    type="string",
     *                    description="API Key issued by Playbasis",
     *                ),
     *                @OA\Property(
     *                    property="api_secret",
     *                    type="string",
     *                    description="API Secret issued by Playbasis",
     *                ),
     *            ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function renew_post()
    {
        $required = $this->input->checkParam(array(
            'api_key',
            'api_secret'
        ));
        if ($required) {
            $this->response($this->error->setError('PARAMETER_MISSING', $required), 200);
        }
        $API['key'] = $this->input->post('api_key');
        $API['secret'] = $this->input->post('api_secret');
        $clientInfo = $this->auth_model->getApiInfo($API);
        if ($clientInfo) {
            $token = $this->auth_model->renewToken(array_merge($clientInfo, $API));
            $this->response($this->resp->setRespond($token), 200);
        } else {
            $this->response($this->error->setError('INVALID_API_KEY_OR_SECRET', $required), 200);
        }
    }

    /**
     * @OA\Post(
     *     tags={"Auth"},
     *     path="/Auth/player/{player_id}",
     *     description="Create a new access token from Playbasis",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *                @OA\Property(
     *                    property="api_key",
     *                    type="string",
     *                    description="API Key issued by Playbasis",
     *                ),
     *            ),
     *         ),
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="player_id",
     *         in="path",
     *         description="Player ID as used in client's website",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="password",
     *         in="query",
     *         description="Player's password",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function player_post($player_id)
    {
        $client_task_id = $this->input->post('client_task_id');
        if (!$client_task_id) {
            $this->response(array(
                'result' => md5($client_task_id.time())."1x".md5(time()),
                'token' => md5('faketoken'.time()).md5('faketoken'.time()),
                'refresh_token' => md5('fakerefreshtoken'.time()).md5('fakerefreshtoken'.time())
            ) , 200);
        }

        $required = $this->input->checkParam(array(
            'api_key',
            'password'
        ));

        if ($required) {
            //$this->response($this->error->setError('PARAMETER_MISSING', $required), 200);
            $this->response(array(
                'result' => md5($device_id.time())."2x".md5(time()),
                'token' => md5('faketoken'.time()).md5('faketoken'.time()),
                'refresh_token' => md5('fakerefreshtoken'.time()).md5('fakerefreshtoken'.time())
            ) , 200);
        }

        $clientInfo = $this->auth_model->getApiInfo(array('key' => $this->input->post('api_key')));

        $password = $this->input->post('password');

        $setting = $this->setting_model->retrieveSetting($clientInfo['client_id'], $clientInfo['site_id']);
        if (!$setting) {
            $this->response($this->error->setError('MISSING_SECURITY_POLICY'), 200);

        }

        $password_policy = $setting['password_policy'];
        if (!$password_policy) {
            $this->response($this->error->setError('MISSING_SECURITY_POLICY'), 200);
        }

        $rule = 'trim|required|xss_clean|check_space|max_length[40]';
        // Rule: password_policy.min_char: number
        $minChar = $password_policy['min_char'];
        if (!$minChar || $minChar <= 0) {
            $minChar = 8;
        }
        $minCharRule = "min_length[".$minChar."]";
        $rule = $rule . '|' .  $minCharRule;

        // Rule: password_policy.user_in_password: boolean
        $policyCannotUseUsernameInPassword = $password_policy['user_in_password'];;
        if ($policyCannotUseUsernameInPassword) {
            $rule = $rule . '|callback_word_in_password[' . $player_id . ']';
        }

        // Rule: password_policy.strategy: {}
        // Must be on of these string
        // NOnly
        // AOnly
        // AtLeastOneN
        // AtLeastOneA
        $strategy = $password_policy['strategy'];
        if ($strategy == "NOnly") {
            $rule = $rule . '|numeric';
        } else if ($strategy == "AOnly") {
            $rule = $rule . '|alpha';
        } else if ($strategy == "AtLeastOneN") {
            $rule = $rule . '|callback_require_at_least_number'; 
        } else if ($strategy == "AtLeastOneA") {
            $rule = $rule . '|callback_require_at_least_alphabet'; 
        } else {
            $rule = $rule . '|numeric';
        }

        $this->form_validation->set_rules('password', 'password', $rule);
        if (!$this->form_validation->run()) {
            $this->response($this->error->setError('INVALID_FORMAT', $this->validation_errors()[0]), 200);
            return;
        }


        $clientInfo['key'] = $this->input->post('api_key');
        $pb_player_id = $this->player_model->getPlaybasisId(array_merge($clientInfo, array(
            'cl_player_id' => $player_id
        )));

        if (!$pb_player_id) {
            //$this->response($this->error->setError('USER_NOT_EXIST'), 200);
            $this->response(array(
                'result' => md5($client_task_id.time())."3x".md5(time()),
                'token' => md5('faketoken'.time()).md5('faketoken'.time()),
                'refresh_token' => md5('fakerefreshtoken'.time()).md5('fakerefreshtoken'.time())
            ) , 200);
        } else {
            $clientInfo['pb_player_id'] = $pb_player_id;
            $clientInfo['password'] = do_hash($password);
        }

        $player = $this->player_model->checkPlayerPassword($clientInfo);
        if(!$player) {
            //$this->response($this->error->setError('PASSWORD_INCORRECT'), 200);
            $this->response(array(
                'result' => md5($client_task_id.time())."4x".md5(time()),
                'token' => md5('faketoken'.time()).md5('faketoken'.time()),
                'refresh_token' => md5('fakerefreshtoken'.time()).md5('fakerefreshtoken'.time())
            ) , 200);
        }

        if ($clientInfo) {
            $token = $this->auth_model->generatePlayerToken($clientInfo);
            //$this->response($this->resp->setRespond($token), 200);
            $this->response(array(
                'result' => md5($client_task_id.$token['token']),
                'token' => $token['token'],
                'refresh_token' => $token['refresh_token'],
            ) , 200);
        } else {
            //$this->response($this->error->setError('INVALID_API_KEY_OR_SECRET', $required), 200);
            $this->response(array(
                'result' => md5($client_task_id.time())."5x".md5(time()),
                'token' => md5('faketoken'.time()).md5('faketoken'.time()),
                'refresh_token' => md5('fakerefreshtoken'.time()).md5('fakerefreshtoken'.time())
            ) , 200); 
        }
    }

    public function validatePasswordFormat_post() {

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);  
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');
        $password = $this->input->post('password');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateParam("password", $password);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);

        $setting = $this->setting_model->retrieveSetting(new MongoId($cid), new MongoId($sid));
        if (!$setting) {
            $this->response($this->error->setError('MISSING_SECURITY_POLICY'), 200);
        }

        $password_policy = $setting['password_policy'];
        if (!$password_policy) {
            $this->response($this->error->setError('MISSING_SECURITY_POLICY'), 200);
        }

        $rule = 'trim|required|xss_clean|check_space|max_length[40]';
        // Rule: password_policy.min_char: number
        $minChar = $password_policy['min_char'];
        if (!$minChar || $minChar <= 0) {
            $minChar = 8;
        }
        $minCharRule = "min_length[".$minChar."]";
        $rule = $rule . '|' .  $minCharRule;

        // Rule: password_policy.user_in_password: boolean
        $policyCannotUseUsernameInPassword = $password_policy['user_in_password'];;
        if ($policyCannotUseUsernameInPassword) {
            $rule = $rule . '|callback_word_in_password[' . $player_id . ']';
        }

        // Rule: password_policy.strategy: {}
        // Must be on of these string
        // NOnly
        // AOnly
        // AtLeastOneN
        // AtLeastOneA
        $strategy = $password_policy['strategy'];
        if ($strategy == "NOnly") {
            $rule = $rule . '|numeric';
        } else if ($strategy == "AOnly") {
            $rule = $rule . '|alpha';
        } else if ($strategy == "AtLeastOneN") {
            $rule = $rule . '|callback_require_at_least_number'; 
        } else if ($strategy == "AtLeastOneA") {
            $rule = $rule . '|callback_require_at_least_alphabet'; 
        } else {
            $rule = $rule . '|numeric';
        }

        $this->form_validation->set_rules('password', 'password', $rule);
        if (!$this->form_validation->run()) {
            $this->response($this->error->setError('INVALID_FORMAT', $this->validation_errors()[0]), 200);
            return;
        } else {
            $this->response($this->resp->setRespond(array(
                'result' => 'done'
            )), 200);
        }
    }

    /**
     * @OA\Post(
     *     tags={"Auth"},
     *     path="/Auth/player/{player_id}/renew",
     *     description="Create a new access token from Playbasis and discard the previous one",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *                @OA\Property(
     *                    property="api_key",
     *                    type="string",
     *                    description="API Key issued by Playbasis",
     *                ),
     *            ),
     *         ),
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="player_id",
     *         in="path",
     *         description="Player ID as used in client's website",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="refresh_token",
     *         in="query",
     *         description="Refresh token",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function player_renew_post($player_id)
    {
        $required = $this->input->checkParam(array(
            'api_key',
            'refresh_token'
        ));
        if ($required) {
            $this->response($this->error->setError('PARAMETER_MISSING', $required), 200);
        }

        $clientInfo = $this->auth_model->getApiInfo(array('key' => $this->input->post('api_key')));
        $pb_player_id = $this->player_model->getPlaybasisId(array_merge($clientInfo, array(
            'cl_player_id' => $player_id
        )));

        if (!$pb_player_id) {
            $this->response($this->error->setError('USER_NOT_EXIST'), 200);
        } else {
            $clientInfo['pb_player_id'] = $pb_player_id;
            $clientInfo['key'] = $this->input->post('api_key');
            $clientInfo['refresh_token'] = $this->input->post('refresh_token');
        }

        $player = $this->auth_model->getPlayerToken($clientInfo, $this->input->post('refresh_token'));
        if(!$player) {
            $this->response($this->error->setError('REFRESH_TOKEN_INCORRECT'), 200);
        }

        if ($clientInfo) {
            $token = $this->auth_model->renewPlayerToken($clientInfo);
            $this->response($this->resp->setRespond($token), 200);
        } else {
            $this->response($this->error->setError('INVALID_API_KEY_OR_SECRET', $required), 200);
        }
    }


    public function player_token_terminate_post($player_id)
    {
        $required = $this->input->checkParam(array(
            'api_key',
            'refresh_token'
        ));
        if ($required) {
            $this->response($this->error->setError('PARAMETER_MISSING', $required), 200);
        }

        $clientInfo = $this->auth_model->getApiInfo(array('key' => $this->input->post('api_key')));
        $pb_player_id = $this->player_model->getPlaybasisId(array_merge($clientInfo, array(
            'cl_player_id' => $player_id
        )));

        if (!$pb_player_id) {
            $this->response($this->error->setError('USER_NOT_EXIST'), 200);
        } else {
            $clientInfo['pb_player_id'] = $pb_player_id;
            $clientInfo['key'] = $this->input->post('api_key');
            $clientInfo['refresh_token'] = $this->input->post('refresh_token');
        }

        $player = $this->auth_model->getPlayerToken($clientInfo, $this->input->post('refresh_token'));
        if(!$player) {
            $this->response($this->error->setError('REFRESH_TOKEN_INCORRECT'), 200);
        }

        if ($clientInfo) {
            $token = $this->auth_model->renewPlayerToken($clientInfo);
            $data = array(
                'token' => "terminated"
            );
            $this->response($this->resp->setRespond($data), 200);
            //$this->response($this->resp->setRespond($token), 200);
        } else {
            $this->response($this->error->setError('INVALID_API_KEY_OR_SECRET', $required), 200);
        }
    }

    /**
     * @OA\Post(
     *     tags={"Auth"},
     *     path="/Auth/player/{player_id}/revoke",
     *     description="Revoke existing Access Token",
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="token",
     *         in="query",
     *         description="Access token returned by Playbasis Authentication",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="player_id",
     *         in="path",
     *         description="Player ID as used in client's website",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function player_revoke_post($player_id)
    {
        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);  
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);
        
        $isOldTokenRevokeRequest = $this->public_services_model->isOldTokenRevokeRequest($client_task_id);
        if ($isOldTokenRevokeRequest) {
            $this->response($this->error->setError('ALREADY_PROCESSED_TRANSACTION'), 200);
        }

        if (!$this->validToken){
            $this->response($this->error->setError('INVALID_TOKEN'), 200);
        }

        $pb_player_id = $this->player_model->getPlaybasisId(array_merge($this->validToken, array(
            'cl_player_id' => $player_id
        )));

        if (!$pb_player_id) {
            $this->response($this->error->setError('USER_NOT_EXIST'), 200);
        }

        $this->auth_model->revokePlayerToken(array_merge($this->validToken,array('pb_player_id' => $pb_player_id)));
        $this->public_services_model->logTokenRevokeRequest(new MongoId($cid),new MongoId($sid), $client_task_id, $player_id);

        $this->response($this->resp->setRespond(), 200);
    }

    private function validTelephonewithCountry($number)
    {
        return (!preg_match("/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d| 2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]| 4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/",
            $number)) ? false : true;
    }
    
    /**
     * @OA\Post(
     *     tags={"Auth"},
     *     path="/Auth/player/{player_id}/register",
     *     description="Register a user from client's website as a Playbasis player",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *                @OA\Property(
     *                    property="api_key",
     *                    type="string",
     *                    description="API Key issued by Playbasis",
     *                ),
     *            ),
     *         ),
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="player_id",
     *         in="path",
     *         description="Player ID as used in client's website",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="password",
     *         in="query",
     *         description="Player's password",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="username",
     *         in="query",
     *         description="Player's username",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="email",
     *         in="query",
     *         description="Player's email",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="image",
     *         in="query",
     *         description="Player's profile image",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="phone_number",
     *         in="query",
     *         description="Player's phone number (e.g. +66 xxyyyyzzzz)",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="tags",
     *         in="query",
     *         description="Specific tag(s) to find",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="facebook_id",
     *         in="query",
     *         description="Player's Facebook ID",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="twitter_id",
     *         in="query",
     *         description="Player's Twitter ID",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="first_name",
     *         in="query",
     *         description="Player's first name",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="last_name",
     *         in="query",
     *         description="Player's last name",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         name="gender",
     *         in="query",
     *         description="Player's gender",
     *         required=false,
     *         @OA\Schema(
     *             enum={"1", "2"}
     *         )
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="birth_date",
     *         in="query",
     *         description="Player's date of birth (e.g. YYYY-MM-DD)",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="code",
     *         in="query",
     *         description="Referral code of another player for an invitation",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         name="anonymous",
     *         in="query",
     *         description="Anonymous flag",
     *         required=false,
     *         @OA\Schema(
     *             enum={"0", "1"}
     *         )
     *     ),
     *     @OA\Parameter(
     *         @OA\Schema(
     *             type="string",
     *         ),
     *         name="device_id",
     *         in="query",
     *         description="Player's Device ID to submit verification process",
     *         required=false,
     *     ),
     *     @OA\Parameter(
     *         name="approve_status",
     *         in="query",
     *         description="Approval status of the Player",
     *         required=false,
     *         @OA\Schema(
     *            enum={"approved", "rejected", "pending"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     )
     * )
     */
    public function player_register_post($player_id)
    {
        if (strlen($player_id) < 3 || strlen($player_id) > 50) {
            $this->response($this->error->setError('INVALID_USERNAME_LENGTH'), 200);
        }

        if($this->startsWithNumber($player_id)) { 
            // for english chars + numbers only
            // valid username, alphanumeric & longer than or equals 3 chars
            $this->response($this->error->setError('INVALID_USERNAME_FORMAT'), 200);
        }

        $csid = $this->input->post('csid');
        list($cid, $sid) = split(';', $csid);  
        $transaction_code = $this->input->post('transaction_code');
        $client_task_id = $this->input->post('client_task_id');

        $this->validateParam("csid", $csid);
        $this->validateCSID($cid, $sid);
        $this->validateParam("transaction_code", $transaction_code);
        $this->validateParam("client_task_id", $client_task_id);
        $this->validateTransaction($transaction_code, $cid, $sid, $client_task_id);
        
        $isOldSignUpRequest = $this->public_services_model->isOldSignUpRequest($client_task_id);
        if ($isOldSignUpRequest) {
            $this->response($this->error->setError('ALREADY_PROCESSED_TRANSACTION'), 200);
        }

        $required = $this->input->checkParam(array(
            'api_key',
            'password',
        ));

        if ($required) {
            $this->response($this->error->setError('PARAMETER_MISSING', $required), 200);
        }

        if (!$player_id) {
            $this->response($this->error->setError('PARAMETER_MISSING', array('player_id')), 200);
        }
        $api_key = $this->input->post('api_key');
        $clientInfo = $this->auth_model->getApiInfo(array('key' => $api_key));

        if (!$clientInfo) {
            $this->response($this->error->setError('INVALID_API_KEY_OR_SECRET', $required), 200);
        }

        if (!$this->validClPlayerId($player_id)) {
            $this->response($this->error->setError('USER_ID_INVALID'), 200);
        }

        //get playbasis player id
        $pb_player_id = $this->player_model->getPlaybasisId(array_merge($clientInfo, array(
            'cl_player_id' => $player_id
        )));

        if ($pb_player_id) {
            $this->response($this->error->setError('USER_ALREADY_EXIST'), 200);
        }

        $playerInfo = array(
            'email' => $this->input->post('email') ? $this->input->post('email') : "qa1+" .$player_id. "@playbasis.com" ,
            'image' => $this->input->post('image') ? $this->input->post('image') : "https://www.pbapp.net/images/default_profile.jpg",
            'username' => $this->input->post('username') ? $this->input->post('username') : $player_id,
            'player_id' => $player_id
        );

        //check if username is already exist in this site
        $player = $this->player_model->getPlayerByUsername($clientInfo['site_id'], $playerInfo['username']);
        if ($player) {
            $this->response($this->error->setError('USERNAME_ALREADY_EXIST'), 200);
        }

        //check if email is already exist in this site
        $player = $this->player_model->getPlayerByEmail($clientInfo['site_id'], $playerInfo['email']);
        if ($player) {
            $this->response($this->error->setError('EMAIL_ALREADY_EXIST'), 200);
        }

        $firstName = $this->input->post('first_name');
        if ($this->utility->is_not_empty($firstName)) {
            $playerInfo['first_name'] = $firstName;
        }
        $lastName = $this->input->post('last_name');
        if ($this->utility->is_not_empty($lastName)) {
            $playerInfo['last_name'] = $lastName;
        }
        $nickName = $this->input->post('nickname');
        if ($this->utility->is_not_empty($nickName)) {
            $playerInfo['nickname'] = $nickName;
        }
        $phoneNumber = $this->input->post('phone_number');
        if ($phoneNumber) {
            if ($this->validTelephonewithCountry($phoneNumber)) {
                $playerInfo['phone_number'] = $phoneNumber;
            } else {
                $this->response($this->error->setError('USER_PHONE_INVALID'), 200);
            }
        }
        $playerInfo['tags'] = $this->input->post('tags') && !is_null($this->input->post('tags')) ? explode(',', $this->input->post('tags')) : null;
        $facebookId = $this->input->post('facebook_id');
        if ($facebookId) {
            $playerInfo['facebook_id'] = $facebookId;
        }
        $twitterId = $this->input->post('twitter_id');
        if ($twitterId) {
            $playerInfo['twitter_id'] = $twitterId;
        }
        $instagramId = $this->input->post('instagram_id');
        if ($instagramId) {
            $playerInfo['instagram_id'] = $instagramId;
        }


        // $this->player_model->unlockPlayer($clientInfo['site_id'], $pb_player_id);
        $password = $this->input->post('password');

        $setting = $this->setting_model->retrieveSetting(new MongoId($cid), new MongoId($sid));
        if (!$setting) {
            $this->response($this->error->setError('MISSING_SECURITY_POLICY'), 200);
        }

        $password_policy = $setting['password_policy'];
        if (!$password_policy) {
            $this->response($this->error->setError('MISSING_SECURITY_POLICY'), 200);
        }

        $rule = 'trim|required|xss_clean|check_space|max_length[40]';
        // Rule: password_policy.min_char: number
        $minChar = $password_policy['min_char'];
        if (!$minChar || $minChar <= 0) {
            $minChar = 8;
        }
        $minCharRule = "min_length[".$minChar."]";
        $rule = $rule . '|' .  $minCharRule;

        // Rule: password_policy.user_in_password: boolean
        $policyCannotUseUsernameInPassword = $password_policy['user_in_password'];;
        if ($policyCannotUseUsernameInPassword) {
            $rule = $rule . '|callback_word_in_password[' . $player_id . ']';
        }

        // Rule: password_policy.strategy: {}
        // Must be on of these string
        // NOnly
        // AOnly
        // AtLeastOneN
        // AtLeastOneA
        $strategy = $password_policy['strategy'];
        if ($strategy == "NOnly") {
            $rule = $rule . '|numeric';
        } else if ($strategy == "AOnly") {
            $rule = $rule . '|alpha';
        } else if ($strategy == "AtLeastOneN") {
            $rule = $rule . '|callback_require_at_least_number'; 
        } else if ($strategy == "AtLeastOneA") {
            $rule = $rule . '|callback_require_at_least_alphabet'; 
        } else {
            $rule = $rule . '|numeric';
        }

        $this->form_validation->set_rules('password', 'password', $rule);
        if (!$this->form_validation->run()) {
            $this->response($this->error->setError('INVALID_FORMAT', $this->validation_errors()[0]), 200);
            return;
        }

        if ($password) {
            $playerInfo['password'] = do_hash($password);
        }

        $gender = $this->input->post('gender');
        if ($this->utility->is_not_empty($gender)) {
            $playerInfo['gender'] = $gender;
        }
        $birthdate = $this->input->post('birth_date');
        if ($birthdate) {
            $timestamp = strtotime($birthdate);
            $playerInfo['birth_date'] = date('Y-m-d', $timestamp);
        }
        $approve_status = $this->input->post('approve_status');
        if ($approve_status) {
            $playerInfo['approve_status'] = $approve_status;
        }
        $device_id = $this->input->post('device_id');
        if ($device_id) {
            $playerInfo['device_id'] = $device_id;
        }
        $referral_code = $this->input->post('code');
        $anonymous = $this->input->post('anonymous');

        if ($anonymous && $referral_code) {
            $this->response($this->error->setError('ANONYMOUS_CANNOT_REFERRAL'), 200);
        }

        // check referral code
        $playerA = null;
        if ($referral_code) {
            $playerA = $this->player_model->findPlayerByCode($clientInfo["site_id"], $referral_code,
                array('cl_player_id'));
            if (!$playerA) {
                $this->response($this->error->setError('REFERRAL_CODE_INVALID'), 200);
            }
        }

        //check anonymous feature depend on plan
        if ($anonymous) {
            $clientData = array(
                'client_id' => $clientInfo['client_id'],
                'site_id' => $clientInfo['site_id']
            );
            $result = $this->client_model->checkFeatureByFeatureName($clientData, "Anonymous");
            if ($result) {
                $playerInfo['anonymous'] = $anonymous;
            } else {
                $this->response($this->error->setError('ANONYMOUS_NOT_FOUND'), 200);
            }
        }

        // get plan_id
        $plan_id = $this->client_model->getPlanIdByClientId($clientInfo["client_id"]);
        try {
            $player_limit = $this->client_model->getPlanLimitById(
                $this->client_plan,
                "others",
                "player");
        } catch (Exception $e) {
            $this->response($this->error->setError('INTERNAL_ERROR'), 200);
        }

        $pb_player_id = $this->player_model->createPlayer(array_merge($clientInfo, $playerInfo), $player_limit);

        if ($pb_player_id) {
            $this->player_model->logPlayerPassword($pb_player_id, array_merge($clientInfo, $playerInfo));
        }
        

        /* trigger reward for referral program (if any) */
        if ($playerA) {

            // [rule] A invite B
            $this->utility->request('engine', 'json', http_build_query(array(
                'api_key' => $api_key,
                'pb_player_id' => $playerA['_id'] . '',
                'action' => ACTION_INVITE,
                'pb_player_id-2' => $pb_player_id . ''
            )));


            // [rule] B invited by A
            $this->utility->request('engine', 'json', http_build_query(array(
                'api_key' => $api_key,
                'pb_player_id' => $pb_player_id . '',
                'action' => ACTION_INVITED,
                'pb_player_id-2' => $playerA['_id'] . ''
            )));
        }


        $this->utility->request('engine', 'json', http_build_query(array(
            'api_key' => $api_key,
            'pb_player_id' => $pb_player_id . '',
            'action' => ACTION_REGISTER
        )));

        /* Automatically energy initialization after creating a new player*/
        foreach ($this->energy_model->findActiveEnergyRewardsById($clientInfo['client_id'], $clientInfo['site_id']) as $energy) {
            $energy_reward_id = $energy['reward_id'];
            $energy_max = (int)$energy['energy_props']['maximum'];
            $batch_data = array();
            if ($energy['type'] == 'gain') {
                array_push($batch_data, array(
                    'pb_player_id' => $pb_player_id,
                    'cl_player_id' => $player_id,
                    'client_id' => $clientInfo['client_id'],
                    'site_id' => $clientInfo['site_id'],
                    'reward_id' => $energy_reward_id,
                    'value' => $energy_max,
                    'date_cron_modified' => new MongoDate(),
                    'date_added' => new MongoDate(),
                    'date_modified' => new MongoDate()
                ));
            } elseif ($energy['type'] == 'loss') {
                array_push($batch_data, array(
                    'pb_player_id' => $pb_player_id,
                    'cl_player_id' => $player_id,
                    'client_id' => $clientInfo['client_id'],
                    'site_id' => $clientInfo['site_id'],
                    'reward_id' => $energy_reward_id,
                    'value' => 0,
                    'date_cron_modified' => new MongoDate(),
                    'date_added' => new MongoDate(),
                    'date_modified' => new MongoDate()
                ));
            }
            if (!empty($batch_data)) {
                $this->energy_model->bulkInsertInitialValue($batch_data);
            }
        }
        if ($pb_player_id) {
            $this->public_services_model->logSignUpRequest(new MongoId($cid),new MongoId($sid), $pb_player_id, $client_task_id, $playerInfo['player_id'], $playerInfo['username']);
            $this->response($this->resp->setRespond(), 200);
        } else {
            $this->response($this->error->setError('LIMIT_EXCEED'), 200);
        }
    }

    // PRIVATE
    private function validClPlayerId($cl_player_id)
    {
        return (!preg_match("/^([a-zA-Z0-9-_=]+)+$/i", $cl_player_id)) ? false : true;
    }

    // PRIVATE
    private function password_validation($client_id, $site_id, $inhibited_str = '')
    {
        $return_status = false;
        $setting = $this->player_model->getSecuritySetting($client_id, $site_id);
        if (isset($setting['password_policy'])) {
            $password_policy = $setting['password_policy'];
            $rule = 'trim|xss_clean';
            if ($password_policy['min_char'] && $password_policy['min_char'] > 0) {
                $rule = $rule . '|' . 'min_length[' . $password_policy['min_char'] . ']';
            }
            if ($password_policy['alphabet'] && $password_policy['numeric']) {
                $rule = $rule . '|callback_require_at_least_number_and_alphabet';
            } elseif ($password_policy['alphabet']) {
                $rule = $rule . '|callback_require_at_least_alphabet';
            } elseif ($password_policy['numeric']) {
                $rule = $rule . '|callback_require_at_least_number';
            }

            if ($password_policy['user_in_password'] && ($inhibited_str != '')) {
                $rule = $rule . '|callback_word_in_password[' . $inhibited_str . ']';
            }
            $this->form_validation->set_rules('password', 'password', $rule);
            if ($this->form_validation->run()) {
                $return_status = true;
            } else {
                $return_status = false;
            }
        } else {
            $return_status = true;
        }
        return $return_status;

    }

    // PRIVATE
    private function validateCSID($cid, $sid) {
        if (!$this->client_model->hasSid($cid, $sid)) {
            $this->response($this->error->setError('CANNOT_FIND_CLIENT_ID'), 200);
        }
    }

    // PRIVATE
    private function validateParam($paramName, $paramValue) {
        if (!$paramValue) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }

    // PRIVATE
    private function validateTransaction($transaction_code, $cid, $sid, $client_task_id) {
        $validCode = md5($cid.$sid.$client_task_id);
        if ($transaction_code != $validCode) {
            $this->response($this->error->setError('INVALID_TRANSACTION'), 200);
        }
    }

    // MUST BE PUBLIC (will be called by CI framework)
    public function require_at_least_number($str)
    {
        if (preg_match('#[0-9]#', $str)) {
            return true;
        }
        $this->form_validation->set_message('require_at_least_number',
            'The %s field require at least one numberic character');
        return false;
    }

    // MUST BE PUBLIC (will be called by CI framework)
    public function require_at_least_alphabet($str)
    {
        if (preg_match('#[a-zA-Z]#', $str)) {
            return true;
        }
        $this->form_validation->set_message('require_at_least_alphabet',
            'The %s field require at least one alphabet character');
        return false;
    }

    // MUST BE PUBLIC (will be called by CI framework)
    public function word_in_password($str, $val)
    {
        if (strpos($str, $val) !== false) {
            $this->form_validation->set_message('word_in_password',
                'The %s field disallow to contain player IDs');
            return false;
        }
        return true;
    }

    // PRIVATE
    private function startsWithNumber($string) {
        return strlen($string) > 0 && ctype_digit(substr($string, 0, 1));
    }
    /*public function test_get()
    {
        echo '<pre>';
        $credential = array(
            'key' => 'abc',
            'secret' => 'abcde'
        );
        echo '<br>getApiInfo:<br>';
        $result = $this->auth_model->getApiInfo($credential);
        print_r($result);
        echo 'site id: ' . $result['site_id']->{'$id'};
        echo '<br>';
        echo 'client id: ' . $result['client_id']->{'$id'};
        echo '<br>';
        echo '<br>generateToken:<br>';
        $result = $this->auth_model->generateToken(array_merge($result, $credential));
        print_r($result);
        echo '<br>findToken:<br>';
        $result = $this->auth_model->findToken($result['token']);
        print_r($result);
        echo '<br>createToken:<br>';
        $result = $this->auth_model->createToken($result['client_id'], $result['site_id']);
        print_r($result);
        echo '<br>createTokenFromAPIKey<br>';
        $result = $this->auth_model->createTokenFromAPIKey($credential['key']);
        print_r($result);
        echo '</pre>';
    }*/
}
