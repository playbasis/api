<!-- HTML for static distribution bundle build -->
<?php
// Check if the environment has a sub-directory
$url = ltrim($_SERVER['REQUEST_URI'], '/');
// Set asset folder
$assetFolder = ($url == 'swagger') ? 'swagger/dist/': 'dist/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Playbasis API Documentation</title>

    <link rel="stylesheet" type="text/css" href="<?= $assetFolder ?>swagger-ui.css" >
    <link rel="stylesheet" type="text/css" href="<?= $assetFolder ?>themes/3.x/theme-monokai.css" />

    <link rel="icon" type="image/png" href="//doc.playbasis.com/images/favicon.ico" sizes="32x32" />
    <link rel="icon" type="image/png" href="//doc.playbasis.com/images/favicon.ico" sizes="16x16" />
    <style>
        html
        {
            box-sizing: border-box;
            overflow: -moz-scrollbars-vertical;
            overflow-y: scroll;
            
        }
        *,
        *:before,
        *:after
        {
            box-sizing: inherit;
            font-family: 'Montserrat', sans-serif !important;
        }
        body
        {
            margin:0;
            background: #fafafa;
            
        }
    </style>
</head>

<body>
<div id="swagger-ui">
</div>
<script src="<?= $assetFolder ?>swagger-ui-bundle.js"> </script>
<script src="<?= $assetFolder ?>swagger-ui-standalone-preset.js"> </script>
<script>
    let url;
    let swaggerPath = 'swagger.json';
    if (location.pathname.substring(1).split('/')[0] !== 'swagger') {
        url = window.location.href + swaggerPath;
    } else {
        url = window.location.origin + location.pathname + swaggerPath;
    }

    window.onload = function() {
        const ui = SwaggerUIBundle({
            url: url,
            dom_id: '#swagger-ui',
            deepLinking: true,
            docExpansion: 'none',
            presets: [
                SwaggerUIBundle.presets.apis,
                SwaggerUIStandalonePreset
            ],
            plugins: [
                SwaggerUIBundle.plugins.DownloadUrl
            ],
            layout: "StandaloneLayout"
        });
        window.ui = ui;
    }
</script>
</body>
</html>