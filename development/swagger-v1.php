<?php

/**
 * @OA\Info(
 *     version="1.1",
 *     title="Playbasis API",
 *     description="API Explorer",
 *     termsOfService="",
 *     @OA\Contact(
 *         email="support@playbasis.com"
 *     ),
 * )
 *
 *
 * @OA\SecurityScheme(
 *     securityScheme="apiKey",
 *     type="apiKey",
 *     in="query",
 *     name="api_key",
 * ),
 */

/**
 * @OA\SecurityScheme(
 *     securityScheme="token",
 *     type="apiKey",
 *     in="query",
 *     name="token",
 * ),
 * security={
 *         {
 *            "apiKey": {},
 *            "token": {}
 *         }
 * }
 */
