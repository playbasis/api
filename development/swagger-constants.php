<?php

define('DEVELOPMENT', '{protocol}://local-api');
define('PREVIEW', 'https://sandbox-api.pbapp.net');
define('PLAYBOOK_PREVIEW', 'https://playbook-api.pbapp.net');
define('PRODUCTION', 'https://[YourCustomApiEndpoint].pbapp.net');
