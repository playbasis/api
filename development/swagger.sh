#!/bin/bash

info="./swagger-v1.php"
readFrom="../application/controllers"
constants="./swagger-constants.php"
path="../swagger/swagger.json"

php ./vendor/bin/openapi --bootstrap $constants --output $path $info $readFrom --format json

echo "OpenAPI V3: Documentation rendered in $path"