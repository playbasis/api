var express = require("express"),
  http = require("http"),
  io = require("socket.io");

var app = express();

// all environments
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.set("port", process.env.PORT || 9000);
var server = http.createServer({}, app);
io = io.listen(server);
io.set("origins", "*:*");
server.listen(app.get("port"), function () {
  console.log("Express server listening on port " + app.get("port"));
});

io.sockets.on("connection", function (socket) {
  socket.on("disconnect", function () {});
  socket.on("op", function (payload) {
    console.log(payload);
    io.emit("op", payload);
  });
});
